---
sidebar: auto
metaTitle: Meltano in the News
description: See news about meltano, as well as Meltano logo downloads and contact information for the team.
---

# Meltano Press

Meltano related news, logos, and how to get in touch.

## Top Stories

- [**GitLab spins out open source data integration platform Meltano**](https://venturebeat.com/2021/06/30/gitlab-spins-out-open-source-data-integration-platform-meltano/)
- [**2021 Best of Open Source Software Awards**](https://www.globenewswire.com/news-release/2021/10/18/2315816/0/en/2021-Best-of-Open-Source-Software-Awards-Identify-the-Most-Groundbreaking-Products-Available-to-Developers-and-IT-Organizations.html)
- [**Meltano Project Gains Momentum After Spinout from GitLab**](https://www.prweb.com/releases/meltano_project_gains_momentum_after_spinout_from_gitlab/prweb18395580.htm)


## Podcasts
- [**Hashmap on Tap**](https://www.hashmapinc.com/hashmapontap/episode/ce3d9168/99-building-a-community-around-dataops-driven-elt-with-douwe-maan-founder-and-ceo-of-meltano)
  - Building a Community Around DataOps-driven ELT with Douwe Maan, Founder and CEO of Meltano- Episode 99
- [**Data Leadership Lessons**](https://podcasts.apple.com/us/podcast/open-source-data-operations-with-douwe-maan-episode-56/id1505108710?i=1000540320210)
  - Open Source Data Operations with Douwe Maan - Episode 56
- [**Data Engineering Podcast**](https://podcasts.apple.com/al/podcast/open-source-production-grade-data-integration-meltano/id1193040557?i=1000484737750)
  - Open Source Production Grade Data Integration With Meltano
- [**Data Engineering Podcast**](https://www.dataengineeringpodcast.com/meltano-singer-data-integration-improvements-episode-200/)
  - Leveling Up Open Source Data Integration With Meltano Hub And The Singer SDK - Episode 200 
- [**Software Engineering Daily**](https://softwareengineeringdaily.com/2021/06/30/meltano-elt-for-dataops-with-douwe-maan/)
  - Meltano: ELT for DataOps with Douwe Maan

## Get in Touch

For press inquiries, please email [hello@meltano.com](mailto:hello@meltano.com).

If you're looking to get in touch with the team for help with Meltano, check out [Getting Help](/docs/getting-help.md).

## Logos

<LogoList />

### CEO Bio
Douwe Maan is the founder and CEO of Meltano, an open source data platform that enables collaboration, efficiency, and visibility.
Programming since age nine, Douwe has been leading software innovation throughout his career - from developing apps to building websites. A SaaS startup founded during his university days led him to join GitLab in 2015 as employee number 10 (after landing Sid Sijbrandij's, GitLab co-founder and CEO, parents as customers), and eventually becoming its first development lead and engineering manager.

In 2019, Douwe initially joined the internal Meltano project at GitLab as Engineering Lead to bring his experience building open source developer tools to the data space and was soon asked to become General Manager to focus on open source data integration.

In 2021, Douwe spun Meltano out of GitLab as an independent startup and raised $4.2M in Seed funding led by GV to bring the entire data lifecycle into the DataOps era.

Passionate about remote working, Douwe spent six months traveling the world, visiting and working with 49 colleagues in 14 countries on 5 continents. Douwe currently lives in Mexico City with his delightful wife, whom he met on his trip around the world, and their two cats.

### Head of Product & Data Bio
Taylor Murphy is the Head of Product and Data of Meltano, an open source data platform that enables collaboration, efficiency, and visibility.
Taylor has been deeply involved in leading and building data-informed teams his entire career. At Concert Genetics he scaled the Data Operations team to enable the management of hundreds of thousands of genetic tests and millions of claims records. 
At GitLab, he was the first data hire where he focused on building and scaling the data organization as the company headed towards its IPO.
He has been involved with Meltano since its inception, acting as the primary customer with whom the team engaged to understand the needs of modern data professionals. 

Taylor is passionate about maximizing the potential of data and building the future of the data profession. Outside of work, he loves spending time with his wife, two boys, and two dogs. 

He graduated from Vanderbilt with a PhD in Chemical and Biomolecular Engineering and the University of Tennessee at Chattanooga with a BS in Chemical Engineering.

Taylor can be found on [Twitter](https://twitter.com/tayloramurphy), [LinkedIn](https://www.linkedin.com/in/tayloramurphy/), and the [Meltano Slack](https://meltano.com/slack).

### Exec and Team Photos
[Exec and Team Photos](https://gitlab.com/meltano/meltano/-/tree/master/docs/src/.vuepress/public/images/press-images)
